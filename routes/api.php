<?php


use Illuminate\Http\Request;
use Dingo\Api\Routing\Router;
use Illuminate\Support\Facades\Route;



Route::post('/words/stat', 'WordsController@stat');

/** @var \Dingo\Api\Routing\Router $api */
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function($api) {
    $api->get('ping', function() { return 'pong';});

    $api->post('/auth/sign-up', 'App\Http\Controllers\AuthController@signUp');
    $api->post('/auth/login', 'App\Http\Controllers\AuthController@login')->name('login');
    $api->get('/auth/logout', 'App\Http\Controllers\AuthController@logout');

    $api->group(['middleware' => 'auth:api'], function ($api) {
        $api->get('/me', 'App\Http\Controllers\AuthController@me');
        $api->get('/refresh', 'App\Http\Controllers\AuthController@refresh');

        $api->get('/phrase', 'App\Http\Controllers\PhraseController@index');
        $api->post('/phrase', 'App\Http\Controllers\PhraseController@store');
    });
});