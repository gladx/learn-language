<?php

namespace App;

class Dictionary
{
    public static function getDic(string $name)
    {
        $words = file_get_contents(base_path() . '/dic/' . $name);
        $arrayWords = explode("\n", $words);
        $trimArrayWordes = array_map(
            function ($word) {
                return strtolower(trim($word));
            },
            $arrayWords
        );

        return $trimArrayWordes;
    }

    public static function W1000() {
        return self::getDic('1000_english_words.txt');
    }

    public static function W100() {
        return self::getDic('100_english_words.txt');
    }

    public static function Oxford3000() {
        return self::getDic('3000_the_oxford.txt');
    }
}
