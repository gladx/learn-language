<?php

namespace App\Http\Controllers;

use App\Phrase;
use Illuminate\Http\Request;

class PhraseController extends Controller
{
    public function index()
    {
        return Phrase::where(['user_id' => $this->user()->id])->paginate();
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:phrases,NULL,'.$this->user()->id . ',user_id'
        ]);

        Phrase::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'user_id' => $this->user()->id
        ]);

        return $this->created();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
