<?php

namespace App\Http\Controllers;

use App\Dictionary;
use Illuminate\Http\Request;

class WordsController extends Controller
{
    public function stat(Request $request) {
        $words =  $request->words;
        $w100 = Dictionary::W100();
        // $w100 = Dictionary::Oxford3000();

        $rawWords = str_word_count($words, 1);
                
        // Steeming to lower
        $cleanWords = [];
        foreach ($rawWords as $word) {
            // ve
            if(in_array($word,['ve', 're', 'th'])) {
                continue;
            }

            if(strlen($word) > 1 || $word == 'a') {
                $lowercase = strtolower($word);
                $trim = trim($lowercase, "\'-");
                $cleanWords[] = $trim;
            }
        }

        $arrayWords = $cleanWords;

        $wordsUnique = array_count_values($arrayWords);
        $words_found = array_intersect($w100, $arrayWords);
        $countAggregate = array_reduce($words_found, function ($sum, $word) use ($wordsUnique) {
            return $sum += ($wordsUnique[$word] ?? 0);
        });

        $words_not_found = array_diff(array_keys($wordsUnique), array_values($words_found));

        $words_not_found = array_values($words_not_found);
        $words_not_found_fix = [];
        array_walk($words_not_found,
            function ($word) use($wordsUnique, &$words_not_found_fix) {
                $words_not_found_fix[$word] = $wordsUnique[$word];
            }
        );

        $countWords = str_word_count($words);
        $countUniqueWords = count($wordsUnique);
        $found_in = count($words_found);
        $not_found = $countUniqueWords - $found_in;

        return response()->json([
            'count' => $countWords,
            'found_agriggate' => $countAggregate,
            'ratio_agriggate' => number_format(($countAggregate / $countWords) * 100, 2),
            'count_unique' => $countUniqueWords,
            'found' => $found_in,
            'ratio' => number_format(($found_in / $countUniqueWords) * 100, 2),
            'not_found' => $not_found,
            'words_not_found' => $words_not_found_fix,
            'words_found' => $words_found,
            'words_unique' => $wordsUnique,
            'words' => $arrayWords,
        ]);
    }
}
